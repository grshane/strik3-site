---
title: Top 3 Valuable Soft Skills for Scientists: How to find them, refine, and show off to recruiters to land your dream job.
description: 
date: 17:34 07/04/2014
taxonomy:
    category: blog
    tag: 
author:
    name: Danielle Minteer
    bio: Danielle has her PhD in Bioengineering from the University of Pittsburgh and wants to do what she can to get her fellow PhDs the stellar careers they deserve.
    twitter: hellostrik3
---

## 1. The ability to have a human conversation. 
I’ve interviewed Ivy League, engineering, highly qualified candidates who do not have this basic, but super important, skill.  Absolutely not all of them, but some either totally dominated the conversation, semeed too arrogant, or talked about too technical information.  I have left interviews wishing nothing but to distance myself and my clients far far away from such oblivious candidates. You can be qualified, but if you treat a recruiter disrespectfully and aren’t self-aware enough, they are not going to want to pass you on to their clients. (Also, this is something that you will definitely be carrying over into your personal lives.)  

### How to find this skill within yourself:  
Talk to people. In person. Catch up with a friend over coffee, have lunch with your lab mates and co-workers; ask how their weekends went, do they have any fun vacations planned for this summer, how their families are doing, have they discovered any good books/podcasts/tv shows/artists lately? And, here’s the important part, listen and remember. This may just be a “tip on how to be a better person” and may seem basic, but I can’t tell you how many times I’ve had a conversation with a candidate and I can tell they are not listening, partly because they are not making eye contact, but also because the next thing they say is totally off-subject. Challenge yourself to make eye contact and to think of a response deeper than just a simple “yeah” or a stare into space. 

### There it is! Make it better: 
After chatting with your friends, take a step back and recap the convo internally. Did they ask you more questions than you asked them? Did you tell them every detail about your week, but learned nothing about theirs? Did the other person’s eyes glaze when you started talking about the novel peptide you’re beginning to test in vivo?

[Aside: when I am a candidate interviewing for a job, the morning of, I always check the local and national news. Did the local NHL team win last night? Is there a pie festival in town this weekend? Did Congress pass a controversial bill? I also make sure to brush up on the biotech news: check out FierceBiotech and Endpoints. Get their emails and follow their Twitter. It’s great when you can have a personal conversation with the interviewer, not just about your science projects.]

### You’re awesome, show it off: 
Subtlety and awareness is key with this one. Are you treating the person on the other end of the convo with the respect and attention you would want somebody to pay towards you? When you have a phone conversation with the recruiter, if they didn’t first, ask if this is still a good time for them to talk. Ask them how they are, how their day is so far. Ask questions about the client, how they feel about this client, thank them when the conversation is over. Follow up with a thank you email. When talking to the employer, always ask questions. Come up with a list of 10-15 questions to ask before the interview - they may answer 10 of them during the conversation, but you’ll still have 5 to ask. 


## 2. Being a self-starter. 

### How to find this skill within yourself: 
What do you find interesting? What do you enjoy in your spare time? What are you passionate about? Do others around you also enjoy these things? Maybe you work on identifying the latest fluorescent microscopy technique during the day, but you’re really into turning traditionally unhealthy meals into clean healthy ones and blogging about it. Maybe you’re a data analyst during the day but have also run 5 marathons. 

### There it is! Make it better: 
Being a self-starter doesn’t always mean starting your own company or getting your own NIH grant funded. Identify what it is that you are super passionate about, I 100% assure that other people out there enjoy it too, and do something about it. One of my grad school friends has a podcast that reviews and critiques movies that he works on in his spare time. While at Oystir, I interviewed a candidate who was still a grad student and she told me something she was most proud of was when she wanted a special $2M microscope for her analyses. Her lab couldn’t fund it, but she worked with the surrounding labs on her floor, above and below, and many chipped in to share the scope. She emptied out a storage closet and turned it into an imaging room. 

### You’re awesome, show it off: 
Nobody else is going to brag for you. Your resume, CV, and interviews are the time and place to show off your accomplishments. I love when candidates tell me about the fun side projects they started in addition to their professional work. This is what separates you from the other 50,000 students also getting their PhD this year. I keep a bottom line on my resume called “Non-Scientific Activities” and this line is reserved for my extra hobbies; not Strik3, not sciVelo, not 4RS, but camping, running marathons, playing music, knitting, painting. This absolutely belongs on a resume; it shows that you can balance a personal life with work. 


## 3. Understand yourself, identify what you bring to the table and what needs more work. 

### How to find this skill within yourself: 
What do you do well during your day job? What do you enjoy most about your day job? Why? What do find yourself dreading on Sunday evenings before starting the week? Why? These don’t have to be super unique qualities - this isn’t always necessarily what separates you from other candidates. But it is incredibly important to know what you actually do well and what you don’t.

Asking a candidate what their worst quality is, is a super common interview question. And no, “caring too much” or “always putting work first” aren’t good answers. Be specific. Really think about these.  

I’m generally a fast-paced, busy person. But I’ve found that if I come home after a long day and sit on my couch at ALL, there is zero chance of me working out that evening. 95% of the time, I will order Indian food, force myself with all of my strength to answer the door when it arrives, scarf it down embarrassingly quickly, watch tv, and fall asleep. The other 5% of the time, I’ll just lay on my couch until I fall asleep. This is a pretty embarrassing and sad reality for an A-type personality like myself to realize about themselves. But know I know that when I come home from work, if I want to get my workout done and prepare my healthy dinner, I need to change into my workout clothes and do it immediately. Knowing this about myself allows me to do what I do well (exercising, being healthy) and work on what I don’t (not letting myself sit around eating junk food all night).

### There it is! Make it better: 
Listen to others. Find a mentor in the lab/office, and outside. Ask those around you for honest feedback. What do you think my 3 strongest qualities are? Do I ever do anything that makes you feel not great? 

When I asked my mentor what my best professional quality is, she told me that I can work with any personality. She also told me (many times over many years) that, when asked a question, I need to stop and critically think about the answer first, instead of blurting out whatever first comes to mind.

### You’re awesome, show it off: 
Share your teammates’ feedback with recruiters. Showing that you care how you make those around you feel gives major bonus points, especially in STEM. At a certain point, there are hundreds of other candidates who can do what you can at the lab bench, but the ability to know what you do well and actually face what you don’t do well and work to improve will set you apart. 

