# Strik3 Site

## Production

On the first deployment.
```
   $ cd ~/webroot/grav
   $ bin/grav install
 ```

## Updating

To update Grav you should use the [Grav Package Manager](http://learn.getgrav.org/advanced/grav-gpm) or `GPM`:

```
$ bin/gpm selfupgrade
```

To update plugins and themes:

```
$ bin/gpm update
```

## Development

To get started with theme development
```
$ cd user/themes/strik3
$ npm install/yarn install
```

Sass compiled using Gulp.js automatically injected via browserSync
```
$ gulp
```
Launches browserSync and runs watch task. 
Change Line 19 in Gulp.js to set your correct local environment. 

